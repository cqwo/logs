package com.cqwo.logs.core.data.rdbs.repository.authors;

import com.cqwo.logs.core.data.rdbs.repository.BaseRepository;
import com.cqwo.logs.core.domain.authors.AuthorLogInfo;

public interface AuthorLogRepository extends BaseRepository<AuthorLogInfo, Integer> {




}
