
package com.cqwo.logs.core.data.rdbs.repository.authors;

import com.cqwo.logs.core.data.rdbs.repository.BaseRepository;
import com.cqwo.logs.core.domain.authors.AuthorSessionInfo;

public interface AuthorSessionRepository extends BaseRepository<AuthorSessionInfo, Integer> {
}