
package com.cqwo.logs.core.data.rdbs.repository.base;

import com.cqwo.logs.core.domain.base.AttachmentInfo;
import com.cqwo.logs.core.data.rdbs.repository.BaseRepository;

public interface AttachmentRepository extends BaseRepository<AttachmentInfo, Integer> {


}