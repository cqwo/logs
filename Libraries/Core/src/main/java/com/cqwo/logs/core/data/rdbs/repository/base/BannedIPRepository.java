package com.cqwo.logs.core.data.rdbs.repository.base;

import com.cqwo.logs.core.domain.base.BannedIPInfo;
import com.cqwo.logs.core.data.rdbs.repository.BaseRepository;

public interface BannedIPRepository extends BaseRepository<BannedIPInfo, Integer> {
}
