package com.cqwo.logs.core.data.rdbs.repository.base;

import com.cqwo.logs.core.domain.base.EventLogInfo;
import com.cqwo.logs.core.data.rdbs.repository.BaseRepository;

public interface EventLogRepository extends BaseRepository<EventLogInfo, Integer> {



}
