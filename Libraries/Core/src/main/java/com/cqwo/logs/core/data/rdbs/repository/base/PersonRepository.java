package com.cqwo.logs.core.data.rdbs.repository.base;

import com.cqwo.logs.core.domain.users.Person;
import com.cqwo.logs.core.data.rdbs.repository.BaseRepository;

public interface PersonRepository extends BaseRepository<Person, Integer> {


}
