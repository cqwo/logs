package com.cqwo.logs.core.data.rdbs.repository.base;

import com.cqwo.logs.core.domain.base.RegionInfo;
import com.cqwo.logs.core.data.rdbs.repository.BaseRepository;

public interface RegionRepository extends BaseRepository<RegionInfo, Integer> {

    Integer countByRegionId(Integer regionId);

}
