package com.cqwo.logs.core.data.rdbs.repository.base;

import com.cqwo.logs.core.domain.sms.SMSInfo;
import com.cqwo.logs.core.data.rdbs.repository.BaseRepository;

public interface SMSRepository extends BaseRepository<SMSInfo, Integer> {
}
