
package com.cqwo.logs.core.data.rdbs.repository.base;

import com.cqwo.logs.core.domain.base.SYSConfigInfo;
import com.cqwo.logs.core.data.rdbs.repository.BaseRepository;

public interface SYSConfigRepository extends BaseRepository<SYSConfigInfo, Integer> {

    /**
     * 找用户名
     * @param name
     * @return
     */
    SYSConfigInfo findByVarName(String name);



}