
package com.cqwo.logs.core.data.rdbs.repository.users;

import com.cqwo.logs.core.domain.users.CreditLogInfo;
import com.cqwo.logs.core.data.rdbs.repository.BaseRepository;

public interface CreditLogRepository extends BaseRepository<CreditLogInfo, Integer> {
}