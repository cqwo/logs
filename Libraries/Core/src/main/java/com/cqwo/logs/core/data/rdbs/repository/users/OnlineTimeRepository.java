package com.cqwo.logs.core.data.rdbs.repository.users;

import com.cqwo.logs.core.domain.users.OnlineTimeInfo;
import com.cqwo.logs.core.data.rdbs.repository.BaseRepository;

public interface OnlineTimeRepository extends BaseRepository<OnlineTimeInfo, Integer> {
}
