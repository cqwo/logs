package com.cqwo.logs.core.data.rdbs.repository.users;

import com.cqwo.logs.core.domain.users.OnlineUserInfo;
import com.cqwo.logs.core.data.rdbs.repository.BaseRepository;

public interface OnlineUserRepository extends BaseRepository<OnlineUserInfo, Integer> {
}
