package com.cqwo.logs.core.data.rdbs.repository.users;

import com.cqwo.logs.core.data.rdbs.repository.BaseRepository;
import com.cqwo.logs.core.domain.users.UserDetailInfo;

public interface UserDetailRepository extends BaseRepository<UserDetailInfo, Integer> {

}