package com.cqwo.logs.core.data.rdbs.repository.users;

import com.cqwo.logs.core.domain.users.UserRankInfo;
import com.cqwo.logs.core.data.rdbs.repository.BaseRepository;

public interface UserRankRepository extends BaseRepository<UserRankInfo, Integer> {
}

