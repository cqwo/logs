package com.cqwo.logs.data;

import com.cqwo.logs.core.cache.CWMCache;
import com.cqwo.logs.core.config.CWMConfig;
import com.cqwo.logs.core.data.CWMData;
import com.cqwo.logs.core.sms.CWMSMS;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;


@Getter
@Setter
public class DataService {

    @Autowired
    private CWMCache cwmCache;

    @Autowired
    private CWMData cwmData;

    @Autowired
    private CWMSMS cwmSMS;

    @Autowired
    private CWMConfig cwmConfig;


}
