package com.cqwo.logs.web.handler;


import com.cqwo.logs.services.Logs;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service(value = "LogsHandler")
@RabbitListener(queues = "com.link510.logs.message")
public class LogsHander {

    @Autowired
    Logs logs;

    @RabbitHandler
    public void listen(String content) throws Exception { //System.out.println("接收消息:" + content);

        try {
            logs.write("你好呀?" + content);
        } catch (Exception ignored) {

        }

    }

}
